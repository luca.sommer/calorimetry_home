from functions import m_json
from functions import m_pck

path = ""

# Versuch Heat Capacity
# mit den zuvor definierten Funktionen werden jetzt die Sensoren ausgelesen und der erste Versuch durchgeführt

metadata = m_json.get_metadata_from_setup('/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json')
metadata = m_json.add_temperature_sensor_serials('/home/pi/calorimetry_home/datasheets',metadata)

data = m_pck.get_meas_data_calorimetry(metadata)

m_pck.logging_calorimetry(data,metadata,'/home/pi/calorimetry_home/data/data_heat_capacity','/home/pi/calorimetry_home/datasheets')

m_json.archiv_json('/home/pi/calorimetry_home/datasheets','/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json', '/home/pi/calorimetry_home/data/data_heat_capacity')

print('Starte zweiten Versuch')

# Versuch Newton'sches Abkühlungsgesetz
# mit den zuvor definierten Funktionen werden jetzt die Sensoren ausgelesen und der zweite Versuch durchgeführt

metadata = m_json.get_metadata_from_setup('/home/pi/calorimetry_home/datasheets/setup_newton.json')
metadata = m_json.add_temperature_sensor_serials('/home/pi/calorimetry_home/datasheets',metadata)

data = m_pck.get_meas_data_calorimetry(metadata)

m_pck.logging_calorimetry(data,metadata,'/home/pi/calorimetry_home/data/data_newton','/home/pi/calorimetry_home/datasheets')

m_json.archiv_json('/home/pi/calorimetry_home/datasheets','/home/pi/calorimetry_home/datasheets/setup_newton.json', '/home/pi/calorimetry_home/data/data_newton')
